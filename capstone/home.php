<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>DreamSync</title>
  <meta content="" name="description">
  <meta content="" name="keywords">
  <meta content="fm011404@gmail.com" name="dicoding:email">

  <!-- Favicons -->
  <link href="assets-home/img/icon.png" rel="icon">
  <link href="assets-home/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link
    href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
    rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets-home/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets-home/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets-home/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets-home/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets-home/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets-home/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets-home/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets-home/css/style.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
    integrity="sha384-mQ93GR66B00ZXjt0YO5KlohRA5SY2XofNCf9brFpLkUjSh5C1wUKTI+U2D5I2kD" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />

</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center">

      <img src="assets-home/img/icon.png" alt="" class="img-logo">
      <h1 class="logo me-auto"><a href="home.php">DreamSync</a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo me-auto"><img src="assets-home/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="#hero">Home</a></li>
          <li><a class="nav-link scrollto" href="#about-us">About</a></li>
          <li><a class="nav-link scrollto" href="#rate">rate</a></li>
          <li><a class="nav-link scrollto" href="#tips-tidur">tips</a></li>
          <li><a class="nav-link scrollto" href="#song">musik</a></li>
          <li><a class="getstarted scrollto" href="index.php">login</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">

    <div class="container">
      <div class="row">
        <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1"
          data-aos="fade-up" data-aos-delay="200">
          <h1>Tidur anda akan lebih nyenyak dengan DreamSync</h1>
          <h2>DreamSync merupakan aplikasi yang dapat membantu anda mengatasi permasalahan seperti susah tidur,insomnia
            dan yang lainnya.</h2>
          <div class="d-flex justify-content-center justify-content-lg-start">
            <a href="#rate" class="btn-get-started scrollto me-3 btn-rate">rating kepenatan</a>
            <a href="#song" class="btn-get-started scrollto btn-rate">relaksasi tidur</a>
          </div>
        </div>
        <div class="col-md-5 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
          <img src="assets-home/img/Path.png" alt="path" class="path">
          <img src="assets-home/img/hero-img_2.png" class="img-fluid" alt="">
        </div>
      </div>
    </div>

  </section><!-- End Hero -->

  <main id="main">
    <!-- ======= About us Section ======= -->
    <section id="about-us" class="about-us section-bg">
      <div class="container-fluid" data-aos="fade-up">

        <div class="row">

          <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">

            <div class="content">
              <h3><strong>About Us</strong></h3>
              <h2>"MIMPIKAN,YAKINI,WUJUDKAN"</h2>
              <p>
                Menjelajahi mimpi, meneguhkan keyakinan, dan mewujudkan inovasi - bersama Dreamsync, kita merangkul
                perjalanan tanpa batas dalam dunia teknologi untuk meningkatkan kualitas tidur remaja. Hadapi tekanan
                akademis, sosial, dan penggunaan gadget dengan solusi inovatif kami.
              </p>
            </div>

          </div>

          <div class="col-lg-4 align-items-stretch order-1 order-lg-2 img"
            style='background-image: url("assets-home/img/sleep.png");' data-aos="zoom-in" data-aos-delay="150">&nbsp;</div>
        </div>

      </div>
    </section><!-- End About Us Section -->

    <!-- Rate Section -->
    <section id="rate" class="rate section-bg">
      <div class="container-fluid" data-aos="fade-up">

        <div class="row">
          <div class="col-lg-5 align-items-stretch order-2 order-lg-1 img"
            style='background-image: url("assets-home/img/rate.png");' data-aos="zoom-in" data-aos-delay="150">&nbsp;</div>

          <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch order-1 order-lg-2">
            <div class="content">
              <h3><strong>Bagaimana kegiatanmu hari ini?</strong></h3>
              <p>
                Kami memberikan solusi untuk anda menceritakan kegiatan anda pada form rating kepenatan
              </p>
              <h2><strong>Fun Fact</strong></h3>
                <p>
                  Tahukah anda, bahwa tidur adalah hal yang penting bagi pertumbuhan remaja
                </p>

            </div>
            <div class="rating">
              <a href="login-index.php">
                <p>
                  Ayo rating kepenatanmu <i class="fa fa-arrow-right" style="color: white;"></i>
                </p>
              </a>
            </div>
          </div>
        </div>

    </section> <!--End Rate Section-->


    <!-- ======= Tips Tidur Section ======= -->
    <section id="tips-tidur" class="tips-tidur section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Tips tidur berkualitas</h2>
        </div>

        <div class="row justify-content-center">

          <!-- Kolom 1 -->
          <div class="col-xl-3 col-md-6 d-flex align-items-stretch column" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <img src="assets-home/img/image1.png" alt="Lorem Ipsum Image">
              <p>Menghindari alkohol dan kafein yang bisa mengganggu tidur</p>
            </div>
          </div>

          <!-- Kolom 2 -->
          <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-md-0 column" data-aos="zoom-in"
            data-aos-delay="200">
            <div class="icon-box">
              <img src="assets-home/img/image2.png" alt="Lorem Ipsum Image">
              <p>Tidur di kamar yang nyaman</p>
            </div>
          </div>

          <!-- Kolom 3 -->
          <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-md-0 column" data-aos="zoom-in"
            data-aos-delay="300">
            <div class="icon-box">
              <img src="assets-home/img/image3.png" alt="Lorem Ipsum Image">
              <p>membaca buku atau melakukan latihan pernapasan</p>
            </div>
          </div>

        </div>

        <div class="row justify-content-center mt-4 ">

          <!-- Kolom 4 -->
          <div class="col-xl-3 col-md-6 d-flex align-items-stretch column" data-aos="zoom-in" data-aos-delay="400">
            <div class="icon-box">
              <img src="assets-home/img/image4.png" alt="Lorem Ipsum Image">
              <p>Mematikan alat-alat elektronik</p>
            </div>
          </div>

          <!-- Kolom 5 -->
          <div class="col-xl-3 col-md-6 d-flex align-items-stretch  mt-4 mt-md-0 column" data-aos="zoom-in"
            data-aos-delay="400">
            <div class="icon-box">
              <img src="assets-home/img/image5.png" alt="Lorem Ipsum Image">
              <p>Saran tidur yang baik antara 6,7 atau 8 jam</p>
            </div>
          </div>

        </div>


      </div>
    </section><!-- End Tips Tidur Section -->

    <!-- Song Section -->
    <section id="song" class="song">

      <div class="container" data-aos="fade-up">

        <header class="section-header">
          <p>Mari berbaring dan nikmati<i class="fa fa-arrow-right"></i></p>
        </header>

        <div class="row">

          <div class="col-lg-5" style="margin-top: 60px;">
            <img src="assets-home/img/music.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-7 mt-5 mt-lg-0 d-flex">
            <div class="row align-self-center gy-4 justify-content-between"
              style="margin-left: 100px; margin-top: 20px;">

              <div class="col-lg-6">
                <div class="card mb-3 musik custom-height-card">
                  <div class="card-body">
                    <h2 class="card-title">Cepat tidur</h2>
                    <h3 class="card-text">Dengarkan musik</h3>
                    <h3 class="card-text">Favorit kamu</h3>
                    <a href="../capstone/music_podcast/index-music.php">
                      <img src="assets-home/img/vector.png" alt="Deskripsi gambar">
                    </a>
                  </div>
                </div>
              </div>

              <div class="col-lg-6">
                <div class="card mb-3 podcast">
                  <div class="card-body">
                    <h2 class="card-title">Dengarkan sekarang</h2>
                    <a href="../capstone/music_podcast/index-podcast.php">
                      <img src="assets-home/img/podcast.png" alt="Deskripsi gambar">
                    </a>
                  </div>
                </div>
              </div>

              <div class="col-lg-6 offset-lg-6">
                <div class="card custom-card2">
                  <div class="card-body">
                    <h2 class="card-title">Tonton film</h2>
                    <a href="../capstone/Tampilan_video/index-video
                    .php">
                      <img src="assets-home/img/movie.png" alt="Deskripsi gambar">
                    </a>
                    <h3 class="card-text">cerita pendek favoritmu</h3>
                  </div>
                </div>
              </div>

              <!-- </div>
          </div> -->
    </section><!-- End Song Section -->
  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">

            <h3>DreamSync</h3>
            <p>
              Aplikasi Website Pintar Pengoptimalan Pola Tidur menggunakan teknologi untuk membantu seseorang tidur
              lebih baik <br><br>

            </p>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#hero">Home</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#about-us">About us</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#tips-tidur">tips</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#song">musik</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="index.php">login</a></li>
            </ul>
          </div>


          <div class="col-lg-3 col-md-6 footer-links">
          <h4>About Us</h4>
            <p>  Menjelajahi mimpi, meneguhkan keyakinan, dan mewujudkan inovasi - bersama Dreamsync, kita merangkul
                perjalanan tanpa batas dalam dunia teknologi untuk meningkatkan kualitas tidur remaja. Hadapi tekanan
                akademis, sosial, dan penggunaan gadget dengan solusi inovatif kami.</p>
          </div>

        </div>
      </div>
    </div>

  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
      class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets-home/vendor/aos/aos.js"></script>
  <script src="assets-home/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets-home/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets-home/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets-home/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets-home/vendor/waypoints/noframework.waypoints.js"></script>
  <script src="assets-home/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assets-home/js/main.js"></script>

</body>

</html>