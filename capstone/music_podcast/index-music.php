<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>My-spotify-Clone</title>
    <link rel="stylesheet" href="css/style.css" />
    <link
      rel="shortcut icon"
      href="images/page-music/footer-icon.png"
      type="image/x-icon"
    />
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor"
      crossorigin="anonymous"
    />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap"
      rel="stylesheet"
    />
    <script src="js/script.js" defer></script>
  </head>
  <body>
    <div class="container-md">
      <nav class="navbar navbar-expand-lg">
        <div class="container-fluid">
          <a class="navbar-brand" href=""
            ><img src="images/page-music/footer-icon.png" alt=""
          /></a>
          <button
            class="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link active" style="color: white; padding: 0em 1em" aria-current="page" href="../home.php">Home</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <div class="main">
        <div class="audio audio1 dark">
            <a href="music3.php"><img src="images/page-music/hindia.png" alt=""></a>
            <h2>Hindia</h2>
            <p>Deretan musik hindia terpopuler saat ini.</p>
        </div>
        <div class="audio audio1 dark">
          <a href="music1.php"><img src="images/page-music/music-1.jpg" alt=""></a>
            <h2>CAS Till i sleep</h2>
            <p>Deretan musik CAS terpopuler</p>
        </div>
        <div class="audio audio1 dark">
          <a href="music2.php"><img src="images/page-music/music-2.jpg" alt=""></a>
            <h2>Indie</h2>
            <p>Dengarkan musik indie yang tersedia.</p>
        </div>
        <div class="audio audio1 dark">
          <a href="music4.php"><img src="images/page-music/music-4.jpeg" alt=""></a>
            <h2>ColdPlay</h2>
            <p>Deretan Lagu Coldplay Terpopuler saat ini.</p>
        </div>
        <div class="audio audio1 dark">
          <a href="music5.php"><img src="images/page-music/music-5.jpg" alt=""></a>
            <h2>Payung Teduh</h2>
            <p>Deretan Lagu Payung Teduh Terpopuler saat ini.</p>
        </div>
        <div class="audio audio1 dark">
          <a href="music4.php"><img src="images/page-music/music-4.jpeg" alt=""></a>
            <h2>ColdPlay</h2>
            <p>Deretan Lagu Coldplay Terpopuler saat ini.</p>
        </div>
        <div class="audio audio1 dark">
          <a href="music4.php"><img src="images/page-music/music-4.jpeg" alt=""></a>
            <h2>ColdPlay</h2>
            <p>Deretan Lagu Coldplay Terpopuler saat ini.</p>
        </div>
        <div class="audio audio1 dark">
          <a href="music4.php"><img src="images/page-music/music-4.jpeg" alt=""></a>
            <h2>ColdPlay</h2>
            <p>Deretan Lagu Coldplay Terpopuler saat ini.</p>
        </div>
        <div class="audio audio1 dark">
          <a href="music4.php"><img src="images/page-music/music-4.jpeg" alt=""></a>
            <h2>ColdPlay</h2>
            <p>Deretan Lagu Coldplay Terpopuler saat ini.</p>
        </div>
      </div>
    </div>
    </body>
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2"
      crossorigin="anonymous"
    ></script>
  </body>
</html>
