// To add more song, just copy the following code and paste inside the array

//   {
//     name: "Here is the music name",
//     artist: "Here is the artist name",
//     img: "image name here - remember img must be in .jpg formate and it's inside the images folder of this project folder",
//     src: "music name here - remember img must be in .mp3 formate and it's inside the songs folder of this project folder"
//   }

//paste it inside the array as more as you want music then you don't need to do any other thing

let allMusic = [
    {
      name: "ColdPlay",
      artist: "Fix You",
      img: "music-20",
      src: "music-20"
    },
    {
      name: "ColdPlay",
      artist: "Paradise",
      img: "music-21",
      src: "music-21"
    },
    {
      name: "ColdPlay",
      artist: "Viva La vida",
      img: "music-22",
      src: "music-22"
    },
    {
      name: "ColdPlay",
      artist: "Yellow",
      img: "music-23",
      src: "music-23"
    },
    {
      name: "ColdPlay",
      artist: "Clocks",
      img: "music-24",
      src: "music-24"
    },
    {
      name: "ColdPlay",
      artist: "The Scientist",
      img: "music-25",
      src: "music-25"
    },
    
    // like this paste it and remember to give comma after ending of this bracket }
    // {
    //   name: "Here is the music name",
    //   artist: "Here is the artist name",
    //   img: "image name here - remember img must be in .jpg formate and it's inside the images folder of this project folder",
    //   src: "music name here - remember img must be in .mp3 formate and it's inside the songs folder of this project folder"
    // }
  ];