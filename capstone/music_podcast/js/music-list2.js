// To add more song, just copy the following code and paste inside the array

//   {
//     name: "Here is the music name",
//     artist: "Here is the artist name",
//     img: "image name here - remember img must be in .jpg formate and it's inside the images folder of this project folder",
//     src: "music name here - remember img must be in .mp3 formate and it's inside the songs folder of this project folder"
//   }

//paste it inside the array as more as you want music then you don't need to do any other thing

let allMusic = [
  {
    name: "nadin amizah",
    artist: "bertaut",
    img: "music-8",
    src: "music-8"
  },
  {
    name: "Rofa",
    artist: "Ibu aku rindu",
    img: "music-9",
    src: "music-9"
  },
  {
    name: "raisa anggiani",
    artist: "kau rumahku",
    img: "music-10",
    src: "music-10"
  },
  {
    name: "dere",
    artist: "kota",
    img: "music-11",
    src: "music-11"
  },
  {
    name: "hanin",
    artist: "Roman picisan",
    img: "music-12",
    src: "music-12"
  },
  {
    name: "nadin amizah",
    artist: "sorai",
    img: "music-13",
    src: "music-13"
  },
];