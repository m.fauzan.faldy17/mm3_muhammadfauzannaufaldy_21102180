// To add more song, just copy the following code and paste inside the array

//   {
//     name: "Here is the music name",
//     artist: "Here is the artist name",
//     img: "image name here - remember img must be in .jpg formate and it's inside the images folder of this project folder",
//     src: "music name here - remember img must be in .mp3 formate and it's inside the songs folder of this project folder"
//   }

//paste it inside the array as more as you want music then you don't need to do any other thing

let allMusic = [
    {
      name: "Payung Teduh",
      artist: "Angin Pujaan Hujan",
      img: "music-26",
      src: "music-26"
    },
    {
      name: "Payung Teduh",
      artist: "Malam",
      img: "music-27",
      src: "music-27"
    },
    {
      name: "Payung Teduh",
      artist: "Menuju Senja",
      img: "music-28",
      src: "music-28"
    },
    {
      name: "Payung Teduh",
      artist: "Resah",
      img: "music-29",
      src: "music-29"
    },
    {
      name: "Payung Teduh",
      artist: "Tidurlah",
      img: "music-30",
      src: "music-30"
    },
    {
      name: "Payung Teduh",
      artist: "Untuk Perempuan Yang Sedang Di Pelukan",
      img: "music-31",
      src: "music-31"
    },
    
    // like this paste it and remember to give comma after ending of this bracket }
    // {
    //   name: "Here is the music name",
    //   artist: "Here is the artist name",
    //   img: "image name here - remember img must be in .jpg formate and it's inside the images folder of this project folder",
    //   src: "music name here - remember img must be in .mp3 formate and it's inside the songs folder of this project folder"
    // }
  ];