// To add more song, just copy the following code and paste inside the array

//   {
//     name: "Here is the music name",
//     artist: "Here is the artist name",
//     img: "image name here - remember img must be in .jpg formate and it's inside the images folder of this project folder",
//     src: "music name here - remember img must be in .mp3 formate and it's inside the songs folder of this project folder"
//   }

//paste it inside the array as more as you want music then you don't need to do any other thing

let allMusic = [
    {
      name: "Hindia",
      artist: "Cincin",
      img: "music-8",
      src: "music-19"
    },
    {
      name: "Hindia",
      artist: "Evaluasi",
      img: "music-8",
      src: "music-18"
    },
    {
      name: "Hindia",
      artist: "Evakuasi",
      img: "music-8",
      src: "music-17"
    },
    {
      name: "Hindia",
      artist: "Secukupnya",
      img: "music-8",
      src: "music-16"
    },
    {
      name: "Hindia",
      artist: "Untuk Apa Untuk Apa",
      img: "music-8",
      src: "music-14"
    },
    {
      name: "Hindia",
      artist: "Apapun Yang Terjadi",
      img: "music-8",
      src: "music-15"
    },
    
    // like this paste it and remember to give comma after ending of this bracket }
    // {
    //   name: "Here is the music name",
    //   artist: "Here is the artist name",
    //   img: "image name here - remember img must be in .jpg formate and it's inside the images folder of this project folder",
    //   src: "music name here - remember img must be in .mp3 formate and it's inside the songs folder of this project folder"
    // }
  ];